#!/usr/bin/env python3
import boto3
import botocore
import logging
import json
import os
import sys
import time
import hashlib
from pprint import pformat


loglevel = os.environ.get('LogLevel', logging.INFO)
logger = logging.getLogger()
logger.setLevel(loglevel)
logging.getLogger('boto3').setLevel(logging.WARNING)
logging.getLogger('botocore').setLevel(logging.WARNING)


cf = boto3.client('cloudformation')


def get_stack_status(stackname):
    stack = cf.describe_stacks(StackName=stackname)
    return stack['Stacks'][0]['StackStatus']


def get_stack_parameters(stackname):
    parameters = dict()
    stack = cf.describe_stacks(StackName=stackname)
    for parameter in stack['Stacks'][0]['Parameters']:
        parameters[parameter['ParameterKey']] = parameter['ParameterValue']
    return parameters


def update_stack_parameters(stackname, parameters, changeset=True, description=None):
    if changeset:
        if description is None:
            description = "updateStackParameters generated changeset"
        unique_hash = hashlib.sha256(str(time.time()).encode('utf-8')).hexdigest()[:20]
        changeset_parameters = dict()
        changeset_parameters['StackName'] = stackname
        changeset_parameters['Description'] = description
        changeset_parameters['UsePreviousTemplate'] = True
        changeset_parameters['Parameters'] = parameters
        changeset_parameters['Capabilities'] = ['CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM']  # See 'InsufficientCapabilities' at https://boto3.readthedocs.io/en/latest/reference/services/cloudformation.html
        changeset_parameters['ChangeSetName'] = "updateStackParamtersLambda-{}".format(unique_hash)
        changeset_parameters['ChangeSetType'] = 'UPDATE'
        if os.getenv('ROLE_ARN', False):
            changeset_parameters['RoleARN'] = os.getenv('ROLE_ARN')
        result = cf.create_change_set(**changeset_parameters)
    else:
        update_parameters = dict()
        update_parameters['StackName'] = stackname
        update_parameters['UsePreviousTemplate'] = True
        update_parameters['Parameters'] = parameters
        update_parameters['Capabilities'] = ['CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM']  # See 'InsufficientCapabilities' at https://boto3.readthedocs.io/en/latest/reference/services/cloudformation.html
        if os.getenv('ROLE_ARN', False):
            update_parameters['RoleARN'] = os.getenv('ROLE_ARN')
        result = cf.update_stack(**update_parameters)
    return result


def update_parameters(old_parameters, new_parameters):
    parameters = list()
    for key, value in old_parameters.items():
        parameter = dict()
        parameter['ParameterKey'] = key
        if key in new_parameters:
            parameter['ParameterValue'] = new_parameters[key]
        else:
            parameter['UsePreviousValue'] = True
        parameters.append(parameter)
    return parameters


def lambda_handler(event, context):
    logger.info("Lambda received the event {}".format(pformat(event)))
    logger.info('Starting...')
    try:
        if event.get('changeset') is None:
            changeset = True
        else:
            changeset = event.get('changeset')
        stackname = event.get('stackname')
        new_parameters = event.get('parameters')
        wait = event.get('wait')
        if not all([stackname, new_parameters]):
            logger.error('One or more variables (stackname, parameters) not found in payload!')
            sys.exit(1)
        old_parameters = get_stack_parameters(stackname)
        for key, value in new_parameters.items():
            if key not in old_parameters:
                logger.error("Parameter '{}' is unknown to the stack '{}'".format(key, stackname))
                sys.exit(1)
        description = event.get('description')
        parameters = update_parameters(old_parameters, new_parameters)
        result = update_stack_parameters(stackname, parameters, changeset, description)
        if not changeset and wait is True:
            done = False
            while not done:
                status = get_stack_status(stackname)
                good = ['UPDATE_COMPLETE']
                bad = ['ROLLBACK_FAILED', 'ROLLBACK_COMPLETE', 'UPDATE_ROLLBACK_FAILED', 'UPDATE_ROLLBACK_COMPLETE']
                if status in good:
                    done = True
                if status in bad:
                    raise Exception("Update stack failed: {}".format(status))
                time.sleep(5)
    except botocore.exceptions.ClientError as ce:
        if ce.response['Error']['Code'] is 'No updates are to be performed.':
            logger.info('No changes detected, not doing anything')
            return 'No changes detected, not doing anything'
        else:
            return logger.error(ce.response['Error']['Code'] + ' -- ' + pformat(ce))
    except Exception as e:
        logger.error('Something went wrong when updating the stack parameters: {}\n'.format(pformat(e)))
        raise
    else:
        logger.info("Done!")
        if changeset is True:
            return "Changeset created: {}".format(result['Id'])
        else:
            return 'Stack updated'


if __name__ == "__main__":
    event = json.loads(sys.argv[1])
    lambda_handler(event, context=None)
